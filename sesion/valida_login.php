<?php 
include $_SERVER['DOCUMENT_ROOT'].'/db_config.php';
/* Este archivo debe manejar la lógica de iniciar sesión */
$correo =  $_POST["correo"];
$contraseña =  $_POST["pass"];
session_start();
if($_SERVER["REQUEST_METHOD"] == "POST")
{
$_SESSION["correo"]= $correo;
$_SESSION["pass"] = $contraseña;
$tablaUsuarios = "SELECT usuario.id ,usuario.contraseña, usuario.correo, usuario.admin FROM usuario";
$correos = array();
$contraseñas = array();
$rs = pg_query( $dbconn, $tablaUsuarios );
    if( $rs )
        {
             if( pg_num_rows($rs) > 0 )
            {
                // Recorrer el resource y mostrar los datos:
                while( $obj = pg_fetch_object($rs) )
                {
                    $correos[$obj->id] =  $obj->correo;
                    $contraseñas[$obj->id] = $obj->contraseña;
                    $roles[$obj->id] =  $obj->admin;
                }
            }
        }
pg_close($dbconn);
$sesion = 0;
for ($i=1; $i <pg_num_rows($rs) ; $i++) 
{ 
   if ($correos[$i] == $correo && $contraseñas[$i] ==$contraseña ) 
   {  
      $_SESSION['rol']=$roles[$i];
      $sesion = 1;
      break;
   }
}

if ($sesion == 1)
{
   $sesion = 0;
   header('Location:/../user/profile.html');
   
}
else
{
   session_destroy();
   header('Location:/../index.html');
}
exit();
}
?>