<?php
/* Este archivo debe validar los datos de registro y manejar la lógica de crear un usuario desde el formulario de registro */
include $_SERVER['DOCUMENT_ROOT'].'/db_config.php';

date_default_timezone_set("America/Santiago");
$name=$_POST['nombre'];
$lastname=$_POST['apellido'];
$email=$_POST['correo'];
$pass=$_POST['password'];
$pass2=$_POST['password2'];
$country=$_POST['pais'];
$columnas="id,nombre,apellido,correo,contraseña,pais,fecha_registro";
$fecha = date('Y-d-N H:i:s');
$admin=1;
#Ultimo ID registrado manualmente = 26 (el 30 se ingreso tambien);
$valores= "26,'$name','$lastname','$email','$pass',$country,'$fecha',$admin";


if ($pass==$pass2){
    
    $registrar="INSERT INTO usuario(id,nombre,apellido,correo,contraseña,pais,fecha_registro,admin) VALUES($valores)";
    $sol_reg = pg_query($dbconn,$registrar);
    if($sol_reg){
        header('Location:../index.html');

    }else
        echo " No se pudo añadir el usuario";

    pg_close($dbconn);
}
?>