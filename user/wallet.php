<?php
/* Este archivo debe manejar la l贸gica para obtener la informaci贸n de la billetera */

include $_SERVER['DOCUMENT_ROOT'].'/db_config.php';
/* Este archivo debe manejar la l贸gica para obtener la informaci贸n del perfil */
$correo = $_SESSION["correo"];
$tablaUsuarios =    "SELECT usuario.id,usuario.nombre as nombre,usuario.apellido,usuario.correo,
                          usuario.fecha_registro, pais.nombre as pais FROM usuario
                    INNER JOIN pais 
                    ON usuario.pais = pais.cod_pais";
$nombres = array();
$apellidos = array();
$correos = array();
$id = 0;
$rs = pg_query( $dbconn, $tablaUsuarios );
    if( $rs )
        {
             if( pg_num_rows($rs) > 0 )
            {
                // Recorrer el resource y mostrar los datos:
                while( $obj = pg_fetch_object($rs) )
                {
                    $nombres[$obj->id] =  $obj->nombre;
                    $apellidos[$obj->id] =  $obj->apellido;
                    $correos[$obj->id] =  $obj->correo;
                }
            }
        }

for ($i=1; $i <pg_num_rows($rs) ; $i++) 
{ 
    if($correos[$i] == $correo )
    {
        $id = $i;
        break;
    }
}
$siglas =  array();
$nombreMoneda = array();
$cantidad = array();
$idMonedas = array();
$tablaBilletera = "SELECT usuario.nombre as nombre, usuario.apellido as apellido, moneda.nombre as nombre_moneda, 
                    usuario_tiene_moneda.balance as cantidad, moneda.sigla as sigla, usuario_tiene_moneda.id_moneda as id_mon
                    FROM usuario_tiene_moneda
                    INNER JOIN usuario
                    ON usuario_tiene_moneda.id_usuario = usuario.id
                    INNER JOIN moneda
                    ON usuario_tiene_moneda.id_moneda = moneda.id 
                    WHERE usuario.nombre = '$nombres[$id]' and usuario.apellido = '$apellidos[$id]'";

$rx = pg_query( $dbconn, $tablaBilletera );
$i=1;
$cantidad_monedas = pg_num_rows($rx); 
if($rx)
{
    if( pg_num_rows($rx) > 0 )
    {
        // Recorrer el resource y mostrar los datos:
        while($obj1 = pg_fetch_object($rx))
        {
            $siglas[$i] = $obj1->sigla;
            $nombreMoneda[$i] = $obj1 ->nombre_moneda;
            $cantidad[$i] = $obj1 ->cantidad;
            $idMonedas[$i]= $obj1 ->id_mon;
            $i++;
        }
    }        

}
$tablaMonedas = array();
for ($i=1; $i <=$cantidad_monedas ; $i++) { 
    $tablaMonedas[$i] = "SELECT * FROM precio_moneda
    WHERE precio_moneda.id_moneda = $idMonedas[$i]
    ORDER BY precio_moneda.fecha DESC
    LIMIT 1";
}
$valorMoneda = array();
for ($i=1; $i <=$cantidad_monedas ; $i++) 
{ 
    $valorMoneda[$i]= pg_fetch_object(pg_query( $dbconn,$tablaMonedas[$i]))->valor;
}
$total = array();
for ($i=1; $i <=$cantidad_monedas ; $i++) 
{
    $total[$i]=$valorMoneda[$i]*$cantidad[$i];
}

pg_close($dbconn);

?>